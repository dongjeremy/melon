package model

type ListItem struct {
	Key      string `json:"key"`
	Hash     string `json:"hash"`
	Fsize    string `json:"fsize"`
	PutTime  string `json:"put_time"`
	MimeType string `json:"mime_type"`
	Type     int    `json:"type"`
	EndUser  string `json:"endUser"`
}

type Params struct {
	Ids []string `json:"ids"`
}
