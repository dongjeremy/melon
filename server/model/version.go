package model

type VersionModel struct {
	Key   string `json:"key"`
	Val   string `json:"val"`
	Group string `json:"group"`
}

type KeyPairs struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type VersionRequest struct {
	Req []KeyPairs `json:"req"`
}

type VersionResponse struct {
	Msg  string `json:"msg"`
	Code string `json:"code"`
}
