package template_func

import (
	"crypto/md5"
	"encoding/hex"
	"strings"
)

func Split(s string, d string) []string {
	arr := strings.Split(s, d)
	return arr
}

func MD5(in string) (string, error) {
	hash := md5.Sum([]byte(in))
	return hex.EncodeToString(hash[:]), nil
}
