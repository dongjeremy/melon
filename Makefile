NAME=melon
VERSION=1.2.0
SOURCE=main.go
LDFLAGS=-ldflags "-s -w"
LLDFLAGS=-ldflags "-linkmode external -extldflags -static -s -w"
BUILD_LINUX=CGO_ENABLED=1 CC=x86_64-linux-musl-gcc GOOS=linux GOARCH=amd64
BUILD_WINDOWS=CGO_ENABLED=1 CC=x86_64-w64-mingw32-gcc CXX=x86_64-w64-mingw32-g++ GOOS=windows GOARCH=amd64
BUILD_DARWIN=CGO_ENABLED=1 GOARCH=amd64 GOOS=darwin
OUTPUT=$(shell pwd)/output

all: dist

fmt: clean
	gofmt -l -w ./

mac: fmt
	${BUILD_DARWIN} go build ${LDFLAGS} -a -o ${OUTPUT}/mac/${NAME} ${SOURCE}
	upx --brute ${OUTPUT}/mac/${NAME}

linux: fmt
	${BUILD_LINUX} go build ${LLDFLAGS} -a -o ${OUTPUT}/linux/${NAME} ${SOURCE}
	upx --brute ${OUTPUT}/linux/${NAME}

window: fmt
	${BUILD_WINDOWS} go build ${LDFLAGS} -a -o ${OUTPUT}/window/${NAME}.exe .
	upx --brute ${OUTPUT}/window/${NAME}.exe

docker: fmt
	docker build -t ${NAME}:${VERSION} -f Dockerfile .
	mkdir -p ${OUTPUT}/docker
	docker save ${NAME}:${VERSION} | gzip > ${OUTPUT}/docker/${NAME}-v${VERSION}.tar.gz

start: fmt
	docker run -d --restart=always -p 80:8088 ${NAME}:${VERSION}

clean:
	rm -rf output/*

dist: mac window linux
	cp -a -f config ${OUTPUT}/window
	cp -a -f config ${OUTPUT}/linux
	cp -a -f config ${OUTPUT}/mac
	find ${OUTPUT} -name .gitkeep -exec rm -fr {} \;
	cd ${OUTPUT}/linux/ && zip -qr ../${NAME}-$(VERSION).linux-amd64.zip .
	cd ${OUTPUT}/window/ && zip -qr ../${NAME}-$(VERSION).window-amd64.zip .
	cd ${OUTPUT}/mac/ && zip -qr ../${NAME}-$(VERSION).darwin-amd64.zip .
