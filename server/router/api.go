package router

import (
	"github.com/gin-gonic/gin"
	v1 "melon/server/api/v1"
)

func InitApiRouter(r *gin.RouterGroup) {
	r.GET("/user/info", (&v1.UserInfo{}).Current)
	r.POST("/users/:id/reset", (&v1.UserInfo{}).Reset)
	r.POST("/users/update", (&v1.UserInfo{}).Update)

	r.GET("/file/list", (&v1.QiniuFile{}).Show)
	r.POST("/file/upload", (&v1.QiniuFile{}).Upload)
	r.DELETE("/file/delete/:filename", (&v1.QiniuFile{}).Delete)
	r.GET("/file/download/:filename", (&v1.QiniuFile{}).Download)
	r.POST("/file/batch/delete", (&v1.QiniuFile{}).BatchDelete)
	r.POST("/version/post", (&v1.Version{}).Update)
	r.POST("/versiondoc/post", (&v1.Version{}).Update)

	r.GET("/myfile/list", (&v1.MyQiniuFile{}).Show)
	r.POST("/myfile/upload", (&v1.MyQiniuFile{}).Upload)
	r.DELETE("/myfile/delete/:filename", (&v1.MyQiniuFile{}).Delete)
	r.GET("/myfile/download/:filename", (&v1.MyQiniuFile{}).Download)
	r.POST("/myfile/batch/delete", (&v1.MyQiniuFile{}).BatchDelete)
}
