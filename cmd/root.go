package cmd

import (
	"melon/server/global"
	"melon/server/utils/yml_config"
	"os"

	"github.com/gookit/color"
	"github.com/spf13/cobra"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "melon",
	Short: "Android版本更新工具",
	Long:  `Android版本更新工具，通过配置文件更新app版本信息`,
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		color.Warn.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/config/melon.yml)")
	rootCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	global.MelonYml = yml_config.CreateYamlFactory()
	global.MelonYml.ConfigFileChangeListen()
}
