package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type UserController struct {
}

func (w *UserController) IndexPage(c *gin.Context) {
	c.HTML(http.StatusOK, "main/index.html", gin.H{
		"title": "Main website",
	})
}
