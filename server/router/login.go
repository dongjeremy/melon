package router

import (
	"github.com/gin-gonic/gin"
	v1 "melon/server/api/v1"
	"melon/server/controller"
)

func InitLoginRouter(r *gin.RouterGroup) {
	r.GET("/loginPage", (&controller.WebPage{}).LoginPage) // 创建Api
	r.POST("/login", (&v1.UserInfo{}).Login)
	r.POST("/logout", (&v1.UserInfo{}).Logout)
	r.GET("/user/current", (&controller.WebPage{}).Current)
}
