package global

import (
	"github.com/casbin/casbin/v2"
	"go.uber.org/zap"
	"gorm.io/gorm"
	"log"
	"melon/server/constant"
	"melon/server/utils/yml_config/ymlconfig_interf"
	"os"
	"strings"
)

var (
	BasePath        string                  // 定义项目的根目录
	ConfigKeyPrefix = "Config_"             //  配置文件键值缓存时，键的前缀
	DateFormat      = "2006-01-02 15:04:05" //  配置文件键值缓存时，键的前缀

	// gorm 数据库客户端，如果您操作数据库使用的是gorm，请取消以下注释，在 bootstrap>init 文件，进行初始化即可使用
	GormDbMysql      *gorm.DB // 全局gorm的客户端连接
	GormDbSqlserver  *gorm.DB // 全局gorm的客户端连接
	GormDbPostgreSql *gorm.DB // 全局gorm的客户端连接
	GormDbSqlite     *gorm.DB // 全局gorm的客户端连接

	MelonYml        ymlconfig_interf.YmlConfigInterf // 全局配置文件指针
	ConfigYml       ymlconfig_interf.YmlConfigInterf // 全局配置文件指针
	ConfigGormv2Yml ymlconfig_interf.YmlConfigInterf // 全局配置文件指针
	ZapLog          *zap.Logger

	//casbin 全局操作指针
	Enforcer *casbin.SyncedEnforcer
)

func init() {
	// 1.初始化程序根目录
	if path, err := os.Getwd(); err == nil {
		// 路径进行处理，兼容单元测试程序程序启动时的奇怪路径
		if len(os.Args) > 1 && strings.HasPrefix(os.Args[1], "-test") {
			BasePath = strings.Replace(strings.Replace(path, `\test`, "", 1), `/test`, "", 1)
		} else {
			BasePath = path
		}
	} else {
		log.Fatal(constant.ErrorsBasePath)
	}
}
