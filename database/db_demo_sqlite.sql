/*
 Navicat Premium Data Transfer

 Source Server         : sqlite3
 Source Server Type    : SQLite
 Source Server Version : 3030001
 Source Schema         : main

 Target Server Type    : SQLite
 Target Server Version : 3030001
 File Encoding         : 65001

 Date: 05/05/2021 20:43:29
*/

PRAGMA foreign_keys = false;

-- ----------------------------
-- Records of sqlite_sequence
-- ----------------------------
BEGIN;
INSERT INTO "sqlite_sequence" VALUES ('tb_users', 1);
COMMIT;

-- ----------------------------
-- Table structure for tb_auth_casbin_rule
-- ----------------------------
DROP TABLE IF EXISTS "tb_auth_casbin_rule";
CREATE TABLE "tb_auth_casbin_rule" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "ptype" varchar(100) DEFAULT '',
  "v0" varchar(100) DEFAULT '',
  "v1" varchar(100) DEFAULT '',
  "v2" varchar(100) DEFAULT '*',
  "v3" varchar(100) DEFAULT '',
  "v4" varchar(100) DEFAULT '',
  "v5" varchar(100) DEFAULT ''
);

-- ----------------------------
-- Records of tb_auth_casbin_rule
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_oauth_access_tokens
-- ----------------------------
DROP TABLE IF EXISTS "tb_oauth_access_tokens";
CREATE TABLE "tb_oauth_access_tokens" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "fr_user_id" INTEGER DEFAULT 0,
  "client_id" INTEGER DEFAULT 1,
  "token" VARCHAR(600) DEFAULT NULL,
  "action_name" VARCHAR(128) DEFAULT '',
  "scopes" VARCHAR(128) DEFAULT '[*]',
  "revoked" TINYINT DEFAULT 0,
  "client_ip" VARCHAR(128) DEFAULT NULL,
  "created_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "expires_at" DATETIME DEFAULT NULL
);

-- ----------------------------
-- Records of tb_oauth_access_tokens
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for tb_users
-- ----------------------------
DROP TABLE IF EXISTS "tb_users";
CREATE TABLE "tb_users" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "user_name" VARCHAR(30) NOT NULL,
  "pass" VARCHAR(128) NOT NULL,
  "nick_name" VARCHAR(30),
  "real_name" VARCHAR(30),
  "sex" TINYINT NOT NULL DEFAULT 1,
  "email" VARCHAR(20),
  "avatar" VARCHAR(50),
  "phone" CHAR(11),
  "status" TINYINT DEFAULT 0,
  "remark" VARCHAR(300),
  "last_login_time" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "last_login_ip" CHAR(30),
  "login_times" INT DEFAULT 0,
  "created_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  "updated_at" DATETIME DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT "rr" UNIQUE ("id")
);

-- ----------------------------
-- Records of tb_users
-- ----------------------------
BEGIN;
INSERT INTO "tb_users" VALUES (1, 'admin', '87d9bb400c0634691f0e3baaf1e2fd0d', '小米', '超级管理员', 0, 'admin@163.com', 'http://cdn.d05660.top/avatar05.png', 18629919851, 1, 'admin123', '2021-05-05 11:10:34', NULL, 0, '2021-05-05 11:10:34', '2021-05-05 11:10:34');
COMMIT;

-- ----------------------------
-- Indexes structure for table tb_auth_casbin_rule
-- ----------------------------
CREATE UNIQUE INDEX "main"."unique_index"
ON "tb_auth_casbin_rule" (
  "ptype" ASC,
  "v0" ASC,
  "v1" ASC,
  "v2" ASC,
  "v3" ASC,
  "v4" ASC,
  "v5" ASC
);

-- ----------------------------
-- Indexes structure for table tb_oauth_access_tokens
-- ----------------------------
CREATE INDEX "main"."oauth_access_tokens_user_id_index"
ON "tb_oauth_access_tokens" (
  "fr_user_id" ASC
);

-- ----------------------------
-- Auto increment value for tb_users
-- ----------------------------
UPDATE "main"."sqlite_sequence" SET seq = 1 WHERE name = 'tb_users';

PRAGMA foreign_keys = true;
