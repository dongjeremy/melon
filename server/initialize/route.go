package initialize

import (
	"github.com/gin-gonic/gin"
	"melon/server/global"
	"melon/server/middleware/authorization"
	"melon/server/router"
)

func InitRoute(Router *gin.Engine) {
	Router.GET("/", func(c *gin.Context) {
		// 指定重定向的URL 通过HandleContext进行重定向到test2 页面显示json数据
		c.Request.URL.Path = "/loginPage"
		Router.HandleContext(c)
	})
	LoginGroup := Router.Group("/")
	{
		router.InitLoginRouter(LoginGroup)
	}
	WebGroup := Router.Group("/main")
	{
		router.InitWebRouter(WebGroup)
	}
	ApiGroup := Router.Group("/api/v1")
	ApiGroup.Use(authorization.CheckTokenAuth())
	{
		router.InitApiRouter(ApiGroup)
	}
	global.ZapLog.Info("router register success")
}
