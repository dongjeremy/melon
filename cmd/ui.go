package cmd

import (
	"github.com/spf13/cobra"
	"melon/server/core"
)

// versionCmd represents the version command
var uiCmd = &cobra.Command{
	Use:   "ui",
	Short: "上传文件并更新配置UI",
	Long:  `上传文件到七牛云并更新数据库配置UI`,
	Run: func(cmd *cobra.Command, args []string) {
		core.RunWindowsServer()
	},
}

func init() {
	rootCmd.AddCommand(uiCmd)
}
