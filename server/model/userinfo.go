package model

type LoginResponse struct {
	Token string `json:"token"`
}

type UserResponse struct {
	Nickname   string `json:"nickname"`
	Sex        string `json:"sex"`
	HeadImgUrl string `json:"headImgUrl"`
}
