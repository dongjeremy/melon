package user_service

import (
	"melon/server/model"
	"melon/server/utils/md5_encrypt"
)

func CreateUserCurdFactory() *UsersCurd {
	return &UsersCurd{}
}

type UsersCurd struct {
}

func (u *UsersCurd) Register(userName, pass, userIp string) bool {
	pass = md5_encrypt.Base64Md5(pass) // 预先处理密码加密，然后存储在数据库
	return model.CreateUserFactory("").Register(userName, pass, userIp)
}

func (u *UsersCurd) ShowUser(userId int64) (*model.UsersModel, error) {
	if user, err := model.CreateUserFactory("").ShowById(userId); err == nil {
		return user, nil
	} else {
		return nil, err
	}
}

func (u *UsersCurd) ResetPass(id, original, password string) bool {
	original = md5_encrypt.Base64Md5(original)
	password = md5_encrypt.Base64Md5(password)
	return model.CreateUserFactory("").ResetPass(id, original, password)
}

func (u *UsersCurd) Update(id int64, nickName, phone, email, remark string, sex int) bool {
	return model.CreateUserFactory("").Update(id, nickName, phone, email, remark, sex)
}
