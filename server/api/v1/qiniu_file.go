package v1

import (
	"github.com/gin-gonic/gin"
	"melon/server/constant"
	"melon/server/model"
	"melon/server/service"
	"melon/server/utils/response"
)

type QiniuFile struct {
}

func (q *QiniuFile) Show(context *gin.Context) {
	prefix := context.DefaultQuery("prefix", "Seek")
	if len(prefix) == 0 {
		prefix = "Seek"
	}
	showList := service.CreateQiniuCurdFactory("Qiniu").GetFileList(prefix + "")
	if showList != nil {
		response.Success(context, constant.CurdStatusOkMsg, showList)
	} else {
		response.Fail(context, constant.CurdSelectFailCode, constant.CurdSelectFailMsg, "")
	}
}

func (q *QiniuFile) Upload(context *gin.Context) {
	formFile, header, _ := context.Request.FormFile("file")
	if uploadFile, err := service.CreateQiniuCurdFactory("Qiniu").UploadFile(header.Filename, formFile, header.Size); err == nil {
		response.Success(context, constant.CurdStatusOkMsg, uploadFile)
	} else {
		response.Fail(context, constant.FilesUploadFailCode, constant.FilesUploadFailMsg, "")
	}
}

func (q *QiniuFile) Download(context *gin.Context) {
	filename := context.Param("filename")
	if downloadFile := service.CreateQiniuCurdFactory("Qiniu").DownloadFile(filename); downloadFile != "" {
		response.Success(context, constant.CurdStatusOkMsg, downloadFile)
	} else {
		response.Fail(context, constant.FilesUploadFailCode, constant.FilesUploadFailMsg, "")
	}
}

func (q *QiniuFile) Delete(context *gin.Context) {
	filename := context.Param("filename")
	if err := service.CreateQiniuCurdFactory("Qiniu").DeleteFile(filename); err == nil {
		response.Success(context, constant.CurdStatusOkMsg, "")
	} else {
		response.Fail(context, constant.FilesUploadFailCode, constant.FilesUploadFailMsg, "")
	}
}

func (q *QiniuFile) BatchDelete(context *gin.Context) {
	var req model.Params
	// 绑定参数
	if err := context.ShouldBindJSON(&req); err != nil {
		response.Fail(context, constant.ValidatorParamsCheckFailCode, constant.ValidatorParamsCheckFailMsg, "")
	}

	factory := service.CreateQiniuCurdFactory("Qiniu")
	for _, value := range req.Ids {
		err := factory.DeleteFile(value)
		if err != nil {
			response.Fail(context, constant.FilesUploadFailCode, constant.FilesUploadFailMsg, "")
			return
		}
	}
	response.Success(context, constant.CurdStatusOkMsg, "")
}
