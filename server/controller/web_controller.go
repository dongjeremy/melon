package controller

import (
	"github.com/gin-gonic/gin"
	"melon/server/constant"
	"melon/server/utils/response"
	"net/http"
	"time"
)

type WebPage struct {
}

func (w *WebPage) IndexPage(c *gin.Context) {
	c.HTML(http.StatusOK, "main/index.html", gin.H{
		"title": "Main website",
	})
}

func (w *WebPage) HomePage(c *gin.Context) {
	c.HTML(http.StatusOK, "main/home.html", gin.H{
		"dates": time.Now(),
	})
}

func (w *WebPage) LoginPage(c *gin.Context) {
	c.HTML(http.StatusOK, "login.html", gin.H{
		"title": "Main website",
	})
}

func (w *WebPage) FileListPage(c *gin.Context) {
	c.HTML(http.StatusOK, "main/file-list.html", gin.H{
		"title": "Main website",
	})
}

func (w *WebPage) MyFileListPage(c *gin.Context) {
	c.HTML(http.StatusOK, "main/my-file-list.html", gin.H{
		"title": "Main website",
	})
}

func (v *WebPage) Current(context *gin.Context) {
	token := context.Query("token")
	if token != "" {
		response.Success(context, constant.CurdStatusOkMsg, "")
	} else {
		response.Fail(context, constant.FilesUploadFailCode, constant.FilesUploadFailMsg, "")
	}
}

func (v *WebPage) UserInfo(c *gin.Context) {
	c.HTML(http.StatusOK, "main/user-info.html", gin.H{
		"title": "Main website",
	})
}

func (v *WebPage) EditPass(c *gin.Context) {
	c.HTML(http.StatusOK, "main/edit-passwd.html", gin.H{
		"title": "Main website",
	})
}
