package cmd

import (
	"fmt"
	"github.com/gookit/color"
	"github.com/spf13/cobra"
	"melon/server/global"
)

// versionCmd represents the version command
var allCmd = &cobra.Command{
	Use:   "all",
	Short: "上传文件并更新配置",
	Long:  `上传文件到七牛云并更新数据库配置`,
	Run: func(cmd *cobra.Command, args []string) {
		uploadFile(global.MelonYml.GetString("Custom.LocalFilename"),
			global.MelonYml.GetString("Qiniu.Bucket"),
			global.MelonYml.GetString("Custom.LocalFilename"),
			global.MelonYml.GetString("Qiniu.AccessKey"),
			global.MelonYml.GetString("Qiniu.SecretKey"))

		urlString := global.MelonYml.GetString("HttpClient.Url")
		domain := global.MelonYml.GetString("Qiniu.Domain")
		filename := global.MelonYml.GetString("Custom.LocalFilename")
		err := updateInfo(urlString, "android_apk_url", fmt.Sprintf("%s/%s", domain, filename))
		if err != nil {
			color.Error.Println("更新url失败")
			return
		}
		err = updateInfo(urlString, "android_app_version", global.MelonYml.GetString("Custom.Version"))
		if err != nil {
			color.Error.Println("更新version失败")
			return
		}
		err = updateInfo(urlString, "android_update_content", global.MelonYml.GetString("Custom.UpdateInfo"))
		if err != nil {
			color.Error.Println("更新info失败")
			return
		}
		err = updateInfo(urlString, "android_update_time", global.MelonYml.GetString("Custom.UpdateTime"))
		if err != nil {
			color.Error.Println("更新时间失败")
			return
		}
		err = updateInfo(urlString, "android_version_control", global.MelonYml.GetString("Custom.CompatibleVersion"))
		if err != nil {
			color.Error.Println("更新信息失败")
			return
		}
		color.Info.Println("更新信息成功")
	},
}

func init() {
	rootCmd.AddCommand(allCmd)
}
