package router

import (
	"github.com/gin-gonic/gin"
	"melon/server/controller"
)

func InitWebRouter(r *gin.RouterGroup) {
	r.GET("/home", (&controller.WebPage{}).HomePage)                         // 创建Api
	r.GET("/index", (&controller.WebPage{}).IndexPage)                       // 创建Api
	r.GET("/version-config", (&controller.VersionJson{}).VersionPage)        // 创建Api
	r.GET("/version-config-doc", (&controller.VersionJson{}).VersionPageDoc) // 创建Api
	r.GET("/apk-file-list", (&controller.WebPage{}).FileListPage)            // 创建Api
	r.GET("/my-file-list", (&controller.WebPage{}).MyFileListPage)           // 创建Api
	r.GET("/user-info", (&controller.WebPage{}).UserInfo)                    // 创建Api
	r.GET("/edit-pass", (&controller.WebPage{}).EditPass)                    // 创建Api
}
