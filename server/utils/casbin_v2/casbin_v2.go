package casbin_v2

import (
	"errors"
	"github.com/casbin/casbin/v2"
	"github.com/casbin/casbin/v2/model"
	gormadapter "github.com/casbin/gorm-adapter/v3"
	"gorm.io/gorm"
	"melon/server/constant"
	"melon/server/global"
	"strings"
	"time"
)

// InitCasbinEnforcer 创建 casbin Enforcer(执行器)
func InitCasbinEnforcer() (*casbin.SyncedEnforcer, error) {
	var tmpDbConn *gorm.DB
	var Enforcer *casbin.SyncedEnforcer
	switch strings.ToLower(global.ConfigGormv2Yml.GetString("Gormv2.UseDbType")) {
	case "mysql":
		if global.GormDbMysql == nil {
			return nil, errors.New(constant.ErrorCasbinCanNotUseDbPtr)
		}
		tmpDbConn = global.GormDbMysql
	case "sqlserver", "mssql":
		if global.GormDbSqlserver == nil {
			return nil, errors.New(constant.ErrorCasbinCanNotUseDbPtr)
		}
		tmpDbConn = global.GormDbSqlserver
	case "postgre", "postgresql", "postgres":
		if global.GormDbPostgreSql == nil {
			return nil, errors.New(constant.ErrorCasbinCanNotUseDbPtr)
		}
		tmpDbConn = global.GormDbPostgreSql
	case "sqlite", "sqlite3":
		if global.GormDbSqlite == nil {
			return nil, errors.New(constant.ErrorCasbinCanNotUseDbPtr)
		}
		tmpDbConn = global.GormDbSqlite
	default:
	}

	prefix := global.ConfigYml.GetString("Casbin.TablePrefix")
	tbName := global.ConfigYml.GetString("Casbin.TableName")

	a, err := gormadapter.NewAdapterByDBUseTableName(tmpDbConn, prefix, tbName)
	if err != nil {
		return nil, errors.New(constant.ErrorCasbinCreateAdaptFail)
	}
	modelConfig := global.ConfigYml.GetString("Casbin.ModelConfig")

	if m, err := model.NewModelFromString(modelConfig); err != nil {
		return nil, errors.New(constant.ErrorCasbinNewModelFromStringFail + err.Error())
	} else {
		if Enforcer, err = casbin.NewSyncedEnforcer(m, a); err != nil {
			return nil, errors.New(constant.ErrorCasbinCreateEnforcerFail)
		}
		_ = Enforcer.LoadPolicy()
		AutoLoad := global.ConfigYml.GetDuration("Casbin.AutoLoadPolicySeconds")
		Enforcer.StartAutoLoadPolicy(time.Second * AutoLoad)
		return Enforcer, nil
	}
}
