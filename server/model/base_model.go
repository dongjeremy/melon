package model

import (
	"fmt"
	"gorm.io/gorm"
	"melon/server/constant"
	"melon/server/global"
	"strings"
)

type BaseModel struct {
	*gorm.DB  `gorm:"-" json:"-"`
	Id        int64  `gorm:"column:id;primarykey" json:"id"`
	CreatedAt string `json:"created_at"` //日期时间字段统一设置为字符串即可
	UpdatedAt string `json:"updated_at"`
}

func UseDbConn(sqlType string) *gorm.DB {
	var db *gorm.DB
	sqlType = strings.Trim(sqlType, " ")
	if sqlType == "" {
		sqlType = global.ConfigGormv2Yml.GetString("Gormv2.UseDbType")
	}
	switch strings.ToLower(sqlType) {
	case "mysql":
		if global.GormDbMysql == nil {
			global.ZapLog.Fatal(fmt.Sprintf(constant.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
		}
		db = global.GormDbMysql
	case "sqlserver":
		if global.GormDbSqlserver == nil {
			global.ZapLog.Fatal(fmt.Sprintf(constant.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
		}
		db = global.GormDbSqlserver
	case "postgres", "postgre", "postgresql":
		if global.GormDbPostgreSql == nil {
			global.ZapLog.Fatal(fmt.Sprintf(constant.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
		}
		db = global.GormDbPostgreSql
	case "sqlite", "sqlite3":
		if global.GormDbSqlite == nil {
			global.ZapLog.Fatal(fmt.Sprintf(constant.ErrorsGormNotInitGlobalPointer, sqlType, sqlType))
		}
		db = global.GormDbSqlite
	default:
		global.ZapLog.Error(constant.ErrorsDbDriverNotExists + sqlType)
	}
	return db
}
