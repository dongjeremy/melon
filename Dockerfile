FROM golang:alpine AS builder

LABEL stage=gobuilder

RUN echo "https://mirrors.nju.edu.cn/alpine/v3.13/main" > /etc/apk/repositories

RUN apk add --update gcc libc-dev

ENV CGO_ENABLED 1
ENV GOOS linux
ENV GOPROXY https://goproxy.cn,direct

WORKDIR /build

ADD go.mod .
ADD go.sum .
RUN go mod download
COPY . .
COPY config /app/config
RUN go build -ldflags="-s -w" -o /app/melon main.go

FROM alpine

RUN apk update --no-cache && apk add --no-cache ca-certificates tzdata
ENV TZ Asia/Shanghai

WORKDIR /app
COPY --from=builder /app/melon /app/melon
COPY --from=builder /app/config /app/config

CMD ["./melon", "ui"]